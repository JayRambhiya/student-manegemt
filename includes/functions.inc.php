<?php
/**
 * When we start a php project we always have to first design the schema of the database
 * after it is done the front end guy will get to know the elements of the form he need 
 * to design and after that tell one guy to fill the clean data for it so the form the
 * project will start looking dynamic once all the backend functions are done the we only have 
 * to call the function and the data will be filled..
 * We always have to have a vision not for a project but for a product...
 */

 //THIS FUNCTONS ARE HELPER FUNCTIONS IT WILL HELP TO REDUCE THE CODE REDUNDANCY AND
 //HELP IN KEEPING THE CODE MANAGED.

 /**
  * This function is made to establish the connection between database and client
  */
function db_connect()

{
    static $connection;

    if(!$connection){
        //try to connect as the databse connection is not established!
        $config = parse_ini_file('config.ini');
        //parse_ini_file returns a array
        $connection = mysqli_connect($config['host'],$config['username'],$config['password'],$config['dbname']);
    }
    //I assure you that $connection has a valid connection established
    if($connection === false){
        echo "In connection fail";
        return mysqli_connect_error();
    }
    return $connection;
}

function db_query_returnId($query) {
    $connection = db_connect();
    $result = mysqli_query($connection,$query);
    $last_inserted_id = mysqli_insert_id($connection);
    return $last_inserted_id;
}
function db_query($query)
{
    $connection = db_connect();
    $result = mysqli_query($connection,$query);
    return $result;
}

function db_error()
{
    $connection = db_connect();
    return mysqli_error($connection);
}
function db_select($query)
{
    $result = db_query($query);
    if($result === false){
        return false;
    }
    $rows = array();
    while($row = mysqli_fetch_assoc($result))
    {
        $rows[] = $row;
    }
    return $rows;
}

function db_quote($value)
{
    $connection = db_connect();
    return mysqli_real_escape_string($connection,$value);
}

function dd($variable)
{
    die(var_dump($variable));
}

function redirect($url){
    header("Location:$url");
}

function add_single_quotes($variable){
    return "'$variable'";
}