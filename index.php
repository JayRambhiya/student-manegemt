<?php
require_once('./includes/functions.inc.php');
?>  
<!doctype html>
<html lang="en">
  <head>
    <title>Student Management</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- https://bootswatch.com/4/minty/bootstrap.css
    https://bootswatch.com/_assets/css/custom.min.css -->
    <!-- Bootswatch CSS-->
    <link rel="stylesheet" href = "https://bootswatch.com/4/flatly/bootstrap.css" media = "screen">
    <!-- <link rel="stylesheet" href="https://bootswatch.com/_assets/css/custom.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/custom.css">
  </head>
  <body>
      <!-- HEADER START -->
      <nav class="navbar navbar-dark bg-primary">
        <a class="navbar-brand text-white">Student Information</a>
        <form class="form-inline">
           <!--<input class="form-control mr-sm-2" type="search" placeholder="Search Student" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>-->
          <!--<a href = "msg.php"class="btn btn-light ml-5">Send Message</a>
          <a href = "email.php"class="btn btn-light ml-5">Send Email</a>-->
          <!--<a href = "new-student.php"class="btn btn-light ml-5">Add Student</a> -->
          <a href = "search-student.php"class="btn btn-light ml-5">Search Student</a>
          <a href = "new-student.php"class="btn btn-light ml-5">Add Student</a>
        </form>
      </nav>
      <!-- HEADER END -->
      <!-- TABLE START -->
      <table class="table table-hover">
        <thead class = "table-dark">
          <tr>
            <th scope="col">Roll No</th>
            <th scope="col">Name</th>
            <th scope="col">Branch</th>
            <th scope="col">Phone Number</th>
            <th scope="col">Email</th>
            <th scope="col">Address</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
<?php
$sql = "SELECT * FROM students";
$rows = db_select($sql);
if($rows === false)
{
    $error = db_error();
    // dd($error);
}
// print_r($row);
foreach($rows as $row):
?>          
          <tr class = "">
              <td><?= $row['id']?></td>
              <td><?= $row['first_name'] . " " . $row['last_name'];?></td>
<?php
$branch_id = $row['branch_id'];
$sql = "SELECT branch_name FROM branches where id = $branch_id";
$branch_name = db_select($sql);
// dd($branch_name[0]['branch_name']);
?>
              <td><?= $branch_name[0]['branch_name'];?></td>
              <td><?= $row['phone_no'];?></td>
              <td><?= $row['email'];?></td>
              <td><?= $row['address'];?></td>
              <td>
                <a href = "edit-student.php?id=<?= $row['id']; ?>" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></a>
                <a href = "view-student.php?id=<?= $row['id']; ?>" class="btn btn-info btn-sm"><i class="fa fa-eye"></i></a>
                <a href="#deleteModal" data-id="<?=$row['id'];?>" class="btn btn-danger btn-sm modal-trigger delete-student"><i class="fa fa-trash"></i></a>
            </td>
          </tr>
<?php
endforeach;
?>
        </tbody>
      </table>
      <!-- TABLE END -->
      <!-- DELETE MODAL START -->
      <div id = "deleteModal"class="modal">
        <div class="modal-dialog" role="">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Delete Student</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <p>Do you want to delete the student?</p>
            </div>

            <div class="modal-footer">
            <form action="delete.php" method = "POST">
                <input type="hidden" name="delete_student_id" id="delete_student_id">
                <button type="submit" class="btn btn-primary" id = "modal-delete-btn">AGREE</button>
            </form>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
            </div>
          </div>
        </div>
      </div>
      <!-- DELETE MODAL END -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="delete_modal.js"></script>
  </body>
</html>