<?php
require_once('./includes/functions.inc.php');
$error_flag = false;
if(isset($_POST['submit']))
{
    // var_dump("true");
    // if(isset($_POST['percentageArray'])){
    //   dd($_POST['percentageArray']);
    // }
    $first_name = db_quote($_POST['first_name']);
    $last_name = db_quote($_POST['last_name']);
    $mother_name = db_quote($_POST['mother_name']);
    $father_name = db_quote($_POST['father_name']);
    $email = db_quote($_POST['email']);
    $phone_no = db_quote($_POST['phone_no']);
    $address = db_quote($_POST['address']);
    $semester = db_quote($_POST['semester']);
    $branch = db_quote($_POST['branch']);
    $percentages = array();
    for($i = 1;$i<=8;$i++){
      array_push($percentages,(isset($_POST["percentage_${i}"]) ? add_single_quotes(db_quote($_POST["percentage_${i}"])): 0.0));
    }
    // dd($percentages);
    /**
     * GETTING BRANCH ID FROM BRANCH
     */
    $branch = add_single_quotes($branch);
    // echo($branch);
    $query = "SELECT id FROM branches WHERE branch_name = {$branch}";
    $result = db_select($query);
    // print_r($result);
    $branch_id = $result[0]['id'];
    // dd($branch_id);
    /**
     * Saving Data to DB
     */
    $first_name = add_single_quotes($first_name);
    $last_name = add_single_quotes($last_name);
    $mother_name = add_single_quotes($mother_name);
    $father_name = add_single_quotes($father_name);
    $email = add_single_quotes($email);
    $phone_no = add_single_quotes($phone_no);
    $address = add_single_quotes($address);
    $semester = add_single_quotes($semester);
    $branch_id = add_single_quotes($branch_id);
    $query = "INSERT INTO students(first_name, last_name, mother_name, father_name, email, phone_no, address, semester,branch_id) VALUES($first_name, $last_name, $mother_name, $father_name, $email, $phone_no, $address, $semester, $branch_id)";
    $result = db_query_returnId($query);
    // dd($result);
    $query = "INSERT INTO `student_percentage`(`stud_id`, `sem_1`, `sem_2`, `sem_3`, `sem_4`, `sem_5`, `sem_6`, `sem_7`, `sem_8`) VALUES ($result,$percentages[0],$percentages[1],$percentages[2],$percentages[3],$percentages[4],$percentages[5],$percentages[6],$percentages[7])";
    $result = db_query($query);
    // dd(db_error());
    if($result)
    {
        redirect("index.php?q=success&op=insert");
        // header("Location: index.php?q=success&op=insert");
    }
    else
    {
        $error_flag = true;
    }
}else{
    // var_dump("false");
}
?>  
<!doctype html>
<html lang="en">
  <head>
    <title>New Student</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href = "https://bootswatch.com/4/flatly/bootstrap.css" media = "screen">
    <!-- <link rel="stylesheet" href = "https://bootswatch.com/4/minty/bootstrap.css" media = "screen"> -->
    <link rel="stylesheet" href="https://bootswatch.com/_assets/css/custom.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/custom.css">
  </head>
  <body>
  <!-- HEADER START -->
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary justify-content-center">
  <h3 class="text-light">Add Student</h3>
  </nav>
  <!-- HEADER END -->
    <div class="container">
      <form action="<?=$_SERVER['PHP_SELF'];?>" id="add-student-form" method="POST" enctype="multipart/form-data">

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>First Name</label>
            <input type="text" class="form-control" id="first_name" name = "first_name" placeholder = "First name">
          </div>
          <div class="form-group col-md-6">
            <label>Last Name</label>
            <input type="text" class="form-control" id="last_name" name = "last_name" placeholder = "Last name">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Mother's Name</label>
            <input type="text" class="form-control" id="mother_name" name = "mother_name" placeholder = "Mother's First name">
          </div>
          <div class="form-group col-md-6">
            <label>Father's Name</label>
            <input type="text" class="form-control" id="father_name" name = "father_name" placeholder = "Father's First name">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="text" class="form-control" id="email" name = "email" placeholder = "Email">
          </div>
          <div class="form-group col-md-6">
            <label>Phone No</label>
            <input type="text" class="form-control" id="phone_no" name = "phone_no" placeholder = "Phone no">
          </div>
        </div>

        <div class="form-group">
          <label>Address</label>
          <input type="text" class="form-control" id="address" placeholder="Address" name = "address">
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Semester</label>
            <select id="semester" class="form-control" name = "semester">
              <option selected>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
            </select>
          </div>

          <div class="form-group col-md-6">
            <label>Branch</label>
            <select id="branch" class="form-control" name = "branch">
<?php
$query = "SELECT branch_name FROM branches";
$branches = db_select($query);
if($branches === false)
{
  $error = db_error();
  dd($error);
}
foreach($branches as $branch):
?>
          <option><?= $branch['branch_name'] ?></option>
<?php
endforeach;
?>
          </select>
        </div>
      </div>  

      <table class="table table-hover col-md-6" id = "percentage_table" name = "percentage_table">
        <thead class = "table-primary">
          <tr>
            <th scope="col">Semester</th>
            <th scope="col">Percentage</th>
          </tr>
        </thead>
        <tbody>
          
        </tbody>
      </table>

      <div class="form-row">
        <button type="submit" class="btn btn-primary" name = "submit" id = "submit">Submit</button>
      </div>
    </form>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script> -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="script.js"></script>
  </body>
</html>