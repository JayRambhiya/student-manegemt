<?php
require_once('./includes/functions.inc.php');
$error_flag = false;
if(isset($_GET['id'])){
    $id = $_GET['id'];
    $rows = db_select("SELECT * FROM students WHERE id = $id");
    // dd($rows);
    if(empty($rows)){
        // redirect("404.html");
    }
}else{
    // dd("Did not get ID");
    redirect("404.html");
}
if(isset($_POST['submit']))
{
  // dd($_POST);
    // var_dump("true");
    $first_name = db_quote($_POST['first_name']);
    $last_name = db_quote($_POST['last_name']);
    $email = db_quote($_POST['email']);
    $phone_no = db_quote($_POST['phone_no']);
    $address = db_quote($_POST['address']);
    $semester = db_quote($_POST['semester']);
    $branch = db_quote($_POST['branch']);
    $percentages = array();
    for($i = 1;$i<=8;$i++){
      array_push($percentages,(isset($_POST["percentage_${i}"]) ? add_single_quotes(db_quote($_POST["percentage_${i}"])): 0.0));
    }
    // $id = $_GET['id'];
    // echo($branch);

    /**
     * GETTING BRANCH ID FROM BRANCH
     */
    $branch = add_single_quotes($branch);
    // echo($branch);
    $query = "SELECT id FROM branches WHERE branch_name = {$branch}";
    $result = db_select($query);
    // dd($result);
    $branch_id = $result[0]['id'];
    // dd($branch_id);
    /**
     * Saving Data to DB
     */
    $first_name = add_single_quotes($first_name);
    $last_name = add_single_quotes($last_name);
    $email = add_single_quotes($email);
    $phone_no = add_single_quotes($phone_no);
    $address = add_single_quotes($address);
    $semester = add_single_quotes($semester);
    $branch_id = add_single_quotes($branch_id);
    // dd($id);
    $query = "UPDATE students SET first_name=$first_name, last_name=$last_name, email=$email,phone_no=$phone_no , address=$address, semester=$semester, branch_id=$branch_id WHERE id = $id";
    // dd($query); 
    
    $result = db_query($query);
    // dd($result);
    $query = "UPDATE `student_percentage` SET `sem_1`=$percentages[0], `sem_2`=$percentages[1], `sem_3`=$percentages[2], `sem_4`=$percentages[3], `sem_5`=$percentages[4], `sem_6`=$percentages[5], `sem_7`=$percentages[6], `sem_8`=$percentages[7] WHERE stud_id=$id";

    $result = db_query($query);

    if($result)
    {
        redirect("index.php?q=success&op=insert");
        // header("Location: index.php?q=success&op=insert");
    }
    else
    {
        $error_flag = true;
    }
}
?>  
<!doctype html>
<html lang="en">
  <head>
    <title>Edit Student</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- https://bootswatch.com/4/minty/bootstrap.css
    https://bootswatch.com/_assets/css/custom.min.css -->
    <!-- Bootswatch CSS-->
    <link rel="stylesheet" href = "https://bootswatch.com/4/flatly/bootstrap.css" media = "screen">
    <link rel="stylesheet" href="https://bootswatch.com/_assets/css/custom.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/custom.css">
  </head>
  <body>
  <!-- HEADER START -->
    
  <nav class="navbar navbar-expand-lg navbar-dark bg-primary justify-content-center">
  <h3 class="text-light">Edit Student</h3>
  </nav>
  <!-- HEADER END -->
    <div class="container">
      <form action="<?=$_SERVER['PHP_SELF']."?id=".$rows[0]['id'];?>" id="edit-student-form" method="POST" enctype="multipart/form-data">

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>First Name</label>
            <input type="text" class="form-control" id="first_name" name = "first_name" value = "<?=$rows[0]['first_name'];?>">
          </div>
          <div class="form-group col-md-6">
            <label>Last Name</label>
            <input type="text" class="form-control" id="last_name" name = "last_name" value = "<?=$rows[0]['last_name'];?>">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Mother's Name</label>
            <input type="text" class="form-control" id="mother_name" name = "mother_name" value = "<?=$rows[0]['mother_name'];?>"placeholder = "Mother's First name">
          </div>
          <div class="form-group col-md-6">
            <label>Father's Name</label>
            <input type="text" class="form-control" id="father_name" name = "father_name" value = "<?=$rows[0]['father_name'];?>" placeholder = "Father's First name">
          </div>
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Email</label>
            <input type="text" class="form-control" id="email" name = "email" value = "<?=$rows[0]['email'];?>">
          </div>
          <div class="form-group col-md-6">
            <label>Phone No</label>
            <input type="text" class="form-control" id="phone_no" name = "phone_no" value = "<?=$rows[0]['phone_no'];?>">
          </div>
        </div>

        <div class="form-group">
          <label>Address</label>
          <input type="text" class="form-control" id="address" placeholder="Address" name = "address" value = "<?=$rows[0]['address'];?>">
        </div>

        <div class="form-row">
          <div class="form-group col-md-6">
            <label>Semester</label>
            <select id="semester" class="form-control" name = "semester">
<?php $semester = $rows[0]['semester'];?>
              <option <?php if($semester == 1) echo 'selected' ?> value = "1">1</option>
              <option <?php if($semester == 2) echo 'selected' ?> value = "2">2</option>
              <option <?php if($semester == 3) echo 'selected' ?> value = "3">3</option>
              <option <?php if($semester == 4) echo 'selected' ?> value = "4">4</option>
              <option <?php if($semester == 5) echo 'selected' ?> value = "5">5</option>
              <option <?php if($semester == 6) echo 'selected' ?> value = "6">6</option>
            </select>
          </div>
<?php
$branch_id = $rows[0]['branch_id'];
// dd($branch_id);
$sql = "SELECT branch_name FROM branches WHERE id = {$branch_id}";
$result = db_select($sql);
// dd($result);
?>
          <div class="form-group col-md-6">
            <label>Branch</label>
            <select id="branch" class="form-control" name = "branch" value = <?= $result[0]['branch_name']?>>
<?php
$query = "SELECT branch_name FROM branches";
$branches = db_select($query);
// dd($branches);
if($branches === false)
{
  $error = db_error();
  dd($error);
}
foreach($branches as $branch):
    if($result[0]['branch_name'] == $branch['branch_name']){
?>
        <option selected value = <?= $branch['branch_name'] ?>> 
          <?= $branch['branch_name'] ?>
         </option>
<?php

    }else{
?>
        <option value = <?= $branch['branch_name'] ?>> 
          <?= $branch['branch_name'] ?>
         </option>
<?php
    }
?>
<?php
endforeach;
?>
          </select>
        </div>
      </div>  

      <table class="table table-hover col-md-6" id = "percentage_table" name = "percentage_table">
        <thead class = "table-primary">
          <tr>
            <th scope="col">Semester</th>
            <th scope="col">Percentage</th>
          </tr>
        </thead>
        <tbody>
<?php
$id = $_GET['id'];
// dd($id);
$query = "SELECT * FROM student_percentage where stud_id = {$id}";
$percentages = db_select($query);
// dd($percentages[0]['sem_2']);
for($i = 1; $i<count($percentages[0]); $i++):
    if($percentages[0]["sem_{$i}"]!= 0):
?>
          <tr>
          <td><?=$i?></td>
          <td><input type="text" class="form-control" id=<?="percentage_${i}"?> placeholder="Percentage" name = <?="percentage_{$i}"?> value=<?=$percentages[0]["sem_{$i}"]?>>
          </td>
          </tr>
<?php
  endif;
endfor;
?>
        </tbody>
      </table>


      <div class="form-row">
        <button type="submit" class="btn btn-primary" name = "submit" id = "submit">Submit</button>
      </div>
    </form>
  </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="script.js"></script>
  </body>
</html>